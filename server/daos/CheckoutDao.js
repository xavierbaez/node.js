var models = require('../models');
var utils = require('../utility/mail');
//var resource = require('../epbroker/CartResource');

//var server = require('server');

var Order = models.order;
var OrderDetails = models.order_details;
var ordid;
Order.hasMany(OrderDetails);


module.exports = {

    /*
     *  Save the Customer's Personal Information in to the database
     */
    savePersonalInfo: function (request, reply) {
        var auth_token = request.headers;
        console.log('Authorization Token : ' + auth_token.authorization);
        var payload = request.payload;
        var lastname = payload.lastname;
        var firstname = payload.firstname;
        var email = payload.email;
        var phone = payload.phone;
        var currentcarrier = payload.currentcarrier;
        console.log('checkoutDao :::: savePersonalInfo() : lastname=' + lastname + ', firstname=' + firstname
            + ', email=' + email + ', phone=' + phone + ', currentcarrier=' + currentcarrier);
        // create an instance of order model
        var order = Order.build(payload);
        order.cart_id = auth_token.authorization;
        order.store = 'ONLINE-STORE';
        order.creditscorerangetype = 'AWESOME-CREDIT';
        console.log(order);
        // persist an instance
        order.save()
            .then(function (order) {
                id = order.id;
                console.log('Order id ' + id + ' Saved Successfully in to the DB :::: ' + order);
                response = {
                    'message': 'Successfully Saved the Customer Personal Information in to the database',
                    'error': 0,
                    'orderdetails': [{orderid: +id}]
                };
                reply(response).code(200);
            });
    },

    /*
     *  Get the Credit Score Range Type from the database
     */
    getCreditScoreRangeType: function (request, reply) {
        var auth_token = request.headers;
        console.log('Authorization Token : ' + auth_token.authorization);
        console.log('checkoutDao :::: getCreditScoreRangeType()');
        //TODO: RSG use this code if you want to read from DB
        //TO DO - GET
        var response = {
            'message': 'Successfully Got the Credit Score Range Type from the database',
            'error': 0,
            items: [{id: 'AWESOME-CREDIT'}, {id: 'AVERAGE-CREDIT'}, {id: 'GOOD-CREDIT'}]
        };
        reply(response).code(200);
    },



    /*
     *  Step 2:
     *  Save the Customer's Billing/Shipping And Credit Card Information to the database
     */
    saveBillAndShipInfo: function (request, reply) {
        var auth_token = request.headers;
        console.log('Authorization Token : ' + auth_token.authorization);
        var payload = request.payload;
        var order_id = payload.orderid;
        var shiptype = payload.shiptype;
        var shipaddress1 = payload.shipaddress1;
        var shipaddress2 = payload.shipaddress2;
        var shipcity = payload.shipcity;
        var shipstate = payload.shipstate;
        var shipzip = payload.shipzip;
        var billaddress1 = payload.billaddress1;
        var billaddress2 = payload.billaddress2;
        var billcity = payload.billcity;
        var billstate = payload.billstate;
        var billzip = payload.billzip;
        var customername = payload.customername;
        var cardno = payload.cardno;
        var expirydate = payload.expirydate;
        var cvv = payload.cvv;
        console.log('checkoutDao :::: saveBillAndShipInfo() : shiptype=' + shiptype + ', shipaddress1=' + shipaddress1
            + ', shipaddress2=' + shipaddress2 + ', shipcity=' + shipcity + ', shipstate=' + shipstate + ', shipzip=' + shipzip
            + ', billaddress1=' + billaddress1 + ', billaddress2=' + billaddress2 + ', billcity=' + billcity,
            +', billstate=' + billstate + ', billzip=' + billzip + ', customername=' + customername,
            +', cardno=' + cardno + ', expirydate=' + expirydate + ', cvv=' + cvv);
        // create an instance of order model
        var order = Order.build(payload);
        console.log(order);
        // update an instance
        Order.update(
            {
                store: payload.store,
                shiptype: shiptype,
                shipaddress1: shipaddress1,
                shipaddress2: shipaddress2,
                shipcity: shipcity,
                shipstate: shipstate,
                shipzip: shipzip,
                billaddress1: billaddress1,
                billaddress2: billaddress2,
                billcity: billcity,
                billstate: billstate,
                billzip: billzip,
                customername: customername,
                cardno: cardno,
                expirydate: expirydate,
                cvv: cvv
            },
            {where: {id: order_id}}
        ).then(function () {
            console.log("Order with id:" + order_id + " updated successfully!");
            response = {
                'message': 'Successfully saved the customer billing and shipping information in to the database',
                'error': 0,
                'orderdetails': [{orderid: +order_id}]
            };
            reply(response).code(200);
        }).catch(function (err) {
            console.log("Order update failed !" + err);
        });
    },
    /*
     * Step 3
     * Save the Credit Score Range Type from the database
     */
    saveCreditScoreRangeType: function (request, reply) {
        var auth_token = request.headers;
        console.log('Authorization Token : ' + auth_token.authorization);
        var payload = request.payload;
        var order_id = payload.cart.id;         // UI sends here because he knows from the Step_1 response
        var order={};
        var cart=payload.cart;
        var items=payload.cart.items;
        //server.log(['debug'], 'server running at:'+server.info.uri);

        // persist an instance
        Order.update(
            {
                customername: payload.nameoncard,
                idtype: payload.customer.idtype,
                idnumber: payload.customer.idnumber,
                creditscorerangetype: payload.customer.creditscorerangetype,
                state: payload.customer.state,
                ssn: payload.customer.ssn,
                dob: payload.customer.dob,
                expdate: payload.customer.expdate
            },
            {where: {id: order_id}}
        ).then(function (entity) {
            order=entity;
            order.id=order_id;
            order.email=payload.customer.email;
            order.firstname=payload.customer.firstname;
            order.lastname=payload.customer.lastname;
            order.total=payload.cart.total;
            console.log("Order with id:" + order_id + " updated successfully!");


        }).catch(function (err) {
            console.log("Order update failed !" + err);
        });
        var ordid=order_id;

        /*Send mail first since we have a order id*/

       utils.sendMail2Customer(order, cart);

        var ep_productIds = [];
        items.forEach(function (value) {
            var orderDetails = OrderDetails.build();
            //orderDetails.id = '';    //TODO: Bharat ???
            orderDetails.order_id = ordid;
            orderDetails.product_id = value.id    // id is really product id or itemid in EP
            ep_productIds.push(value.id);
            orderDetails.price = value.price
            orderDetails.tax = '7.0';
            console.log('Object before the save=' + orderDetails);
            orderDetails.save().then(function (order_detail) {
                console.log('Order detail getting created with id  = ' + order_detail.id);
            });
        });

        // Send the response to the customer
        /*response = {'order_id': id};*/

        // DEPOSIT THE ORDER TO THE EXTERNAL SYSTEM
        /*
         We can continue processing because of nodejs main thread
         */

        console.log('We will add the following itemId to ELASTIC PATH');
        console.log('Item id=' + ep_productIds[0]);
        console.log('Item id=' + ep_productIds[1]);
/* RSG
        resource.getNewAccessToken(function (token) {
            console.log("token id " + token);
            response = {
                'message': 'Successfully updated order with the customers credit/identification/rating info.',
                'error': 0,
                'orderdetails': [{orderid: +order_id}],
                'token':token
            };
            reply(response).code(200);
        });
*/

    }


};