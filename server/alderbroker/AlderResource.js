var request = require('request');
var JWT = require('jsonwebtoken');
var querystring = require('querystring');

var decoded;
var base_uri = 'https://api.test.com';
module.exports = {



    /*
     *  Get list of  product/catalog items from Alder
     */

    getCatalogs: function (auth_token, programId, callback) {
        var message = 'In VMS or IDS getCatalogs() failure!!! ';
        var response = {'message': message, 'error': 500};

        console.log(base_uri + '/programs/api/programs/' + programId +'/catalogs/1');
        console.log('https://app.test.com/programs/api/programs/3425/catalogs/1');
        request({
            headers: {
                'Authorization': 'bearer ' + auth_token,
                'Accept': 'application/json'
            },
            uri: 'https://app.test.com/programs/api/programs/3425/catalogs/1',
            method: 'GET'
        }, function (err, res, body) {
            if (err) {
                callback(response.code(500));
                console.log(message);
            }
            else {
                console.log(body);
                console.log('In VMS or IDS getCatalogs() success..');
                callback(body);
            }
        });
    },

    /*
     *  Get all assets
     */

    getAssets: function (auth_token, programId, callback) {
        var message = 'In VMS or IDS assets failure!!! ';
        var response = {'message': message, 'error': 500};

        console.log(base_uri + '/programs/api/programs/' + programId +'/catalogs/1');
        console.log('https://app.test.com/programs/api/programs/3425/catalogs/1');
        request({
            headers: {
                'Authorization': 'bearer ' + auth_token,
                'Accept': 'application/json'
            },
            uri: 'https://app.test.com/programs/api/programs/3425/catalogs/1/assets',
            method: 'GET'
        }, function (err, res, body) {
            if (err) {
                callback(response.code(500));
                console.log(message);
            }
            else {
                console.log(body);
                console.log('In VMS or IDS getAssets() success..');
                callback(body);
            }
        });
    },




    /*
     *  Get brand new access token from VMS or IDS
     */
    getNewAccessToken: function (callback) {
        var message = 'VMS or IDS getNewAccessToken() failure!!!';
        var response = {'message': message, 'error': 500};

        console.log('In VMSResource getNewAccessToken(): ');
        var postData = querystring.stringify({
            grant_type: 'client_credentials',
            client_id: 'amexcomtest',
            client_secret: 'NKxn&7haNzoHfm[)wVutM.Rft.#|)l0U'
         });
        request({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': postData.length
            },
            uri: base_uri + '/auth/token',
            method: 'POST',
            body:postData

        }, function (err, res, body) {
            if (err) {
                callback('Failed updating cart!!!').code(500);
                console.log('Failed to update cart!!!');
                return;
            } else {
                console.log(body);
                var auth_res = JSON.parse(body);
                access_token = auth_res.access_token;
                token_type = auth_res.token_type;
                console.log('VMS or IDS getNewAccessToken() success..' + access_token);
                callback(access_token);
            }
        });
    },


}













