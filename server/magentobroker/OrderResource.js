
/**
 * Created by rsg on 9/20/2017.
 */
var request = require('request');
var JWT = require('jsonwebtoken');
var querystring = require('querystring');


var magento='http://testing.local.com';
var magento_context = '';
var magento_base_uri = magento + magento_context + '/rest/default/V1';
var magento_auth_token = 'hq6ptcl9c9mv5sta1uoffiy5i68kmtym';




module.exports = {


    getMagentoOrderById: function(orderId,callback) {
        request(magento_base_uri + '/orders/' + orderId,
            {
                json: true, method: 'GET',
                headers: {'Authorization': 'bearer ' + magento_auth_token, 'Content-type': 'application/json'}

            },
            function (err, res, body) {
                if (err) {
                    console.log('Cannot communicate to MAGENTO');
                    callback(res);
                }
                console.log(body);
                callback(body);
            });
    },
    /*
    *  Update Status for notifications. Sends response from here
    */
    updateOrderStatus: function (jsonOrder,reply) {
        var orderId = jsonOrder.order; //'000000005';
        console.log('ProductResource: updateStatus() with order/suborder id: ' + orderId);
        var response = {'message': 'updateStatus..', 'error': 0};

        module.exports.updateOrderStatusDetails(jsonOrder, function (data) {
            console.log('Data is' + data);
            reply(data);
        });
    },


    updateOrderStatusDetails: function (jsonOrder, callback) {

        var response = [];

        request.post({
            url: "http://amex.local.com/rest/default/V1/orders/" + jsonOrder.order + "/comments",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + magento_auth_token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                "statusHistory":{ "comment": jsonOrder.message },
                "params":[]
            },
            json:true
        }, function(error, response, body){
            console.log('error=' + error);
            console.log('response=' + response);
            console.log('body=' + body);
            if (error) {
                callback(response);
                console.log('response=' + response);
            }
            else {
                console.log(body);
                console.log('POST to Magento update order successful..');
                callback(body);
            }

        });
    },

    findDenominationsByProductId: function (productId,reply) {
        console.log('ProductResource: findDenominations() for product id: ' + productId);
        var response = {'message': 'findDenominationsByProductId..', 'error': 0};
        var json = JSON.parse(require('fs').readFileSync('testfiles/denominations.json', 'utf8'));
        reply(json).code(200);


    },

    findFeesByProductId: function (productId,reply) {
        console.log('ProductResource: findFeesByProductId() for product id: ' + productId);
        var response = {'message': 'findFeesByProductId..', 'error': 0};
        var json = JSON.parse(require('fs').readFileSync('testfiles/purchasefee.json', 'utf8'));
        reply(json).code(200);

    },


    findShippingOptionsByProductId: function (productId,reply) {
        console.log('ProductResource: findShippingOptions() for product id: ' + productId);
        var response = {'message': 'findShippingOptionsByProductId..', 'error': 0};
        var json = JSON.parse(require('fs').readFileSync('testfiles/shippingoptions.json', 'utf8'));
        reply(json).code(200);

    },


};


