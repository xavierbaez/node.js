
/**
 * Created by rsg on 9/20/2017.
 */

var httprequest = require('request');

var magento='http://testing.local.com';
var magento_context = '';
var magento_base_uri = magento + magento_context + '/rest/default/V1';  //default was not there prior to 2.1.9
var magento_auth_token = 'hq6ptcl9c9mv5staqwewey5i68kmtym';




module.exports = {


    /*
     *  Get all products from Magento Server via REST API. Sends response from here
     */
    findAllProducts: function (category_id,reply) {
        console.log('ProductResource: findAllProducts() with object id: ' + id);
        var response = {'message': 'findAllProducts..', 'error': 0};

        module.exports.findAllCatalogItems(category_id, function (data) {
                reply(data).code(200);
        });
    },


    findAllCatalogItems: function (searchString, callback) {

        var productList = [];

	      httprequest(magento_base_uri + '/products?searchCriteria[page_size]=12',
            {
                json: true, method: 'GET',
                headers: {'Authorization': 'bearer ' + magento_auth_token, 'Content-type': 'application/json'}
            },
            function (err, res, body) {
                if (err) {
                    console.log('Cannot communicate to MAGENTO');
                    return;
                }
 		        var items = body.items;
                var json=JSON.stringify(body.items);
                console.log(json);
                if (body.items[0] == null) {console.log('Empty'); return; }     // Check for empty links
                var counter=1;
                for (var item in items) {
                    var obj = items[item];
                    console.log('type..........' + obj.id);
                    var Product = {};
                    Product['id'] = counter++;
                    Product['epid'] = obj.id;
                    Product['availability'] = '';
                    Product['name'] = obj.name;
                    Product['description'] = '';
                    Product['retail_price'] = obj.price;
                    Product['sale_price'] = obj.price;
                    Product['promotion'] = 1;
                    Product['size'] = 'Standard';
                    Product['configuration'] = '16 GB';
                    Product['color'] = '';
                    Product['skuid'] = obj.sku;
                    Product['picture'] = '';
                    Product['active'] = obj.status;
                    Product['contract'] = '24 months';
		    Product['picture'] = 'http://ec2-54-202-80-116.us-west-2.compute.amazonaws.com/magento/pub/media/catalog/product/cache/small_image/240x300/beff4985b56e3afdbeabfc89641a4582/' + obj.image_name;
		    // get the attributes 
		    var attributes = obj.custom_attributes;
                    for (var it in attributes) {
                        var ob = attributes[it];
                        console.log(ob.attribute_code);
                        console.log(ob.value);
                        if (ob.attribute_code === 'image') { Product['picture'] ='http://ec2-54-202-80-116.us-west-2.compute.amazonaws.com/magento/pub/media/catalog/product/cache/small_image/240x300/beff4985b56e3afdbeabfc89641a4582/' + ob.value;}
                    }

                    productList.push(Product);
                    counter++;
                    console.log(Product);
                }
                callback(productList);
            });
      },





};


