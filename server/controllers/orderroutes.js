const nodemailer = require('nodemailer');
var authentication=require('../controllers/authentication');
var orderService = require('../services/OrderService');
var Joi= require('joi');

module.exports=[
    {
        method: 'GET',
        path: '/magento/order/{orderid}',
        config: {
            auth: false,
            handler: orderService.getMagentoOrderById,
            description: 'Get an order from Magento',
            notes: 'A GET Request to get a single order from magento',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                params: Joi.object({
                    'orderid': Joi.string().required()
                }).unknown(),
                headers: Joi.object({
                    'Authorization': Joi.string().required()
                }).unknown()
            }
        }
    },
    {
        method: 'POST',
        path: '/services/V1/api/order/status/notifications',
        config: {
            auth: false,
            handler: orderService.updateMagentoOrder,
            description: 'Incoming status notification',
            notes: 'A POST Request for sending status notification',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    order: Joi.string().required(),
                    suborder: Joi.string().required(),
                    status: Joi.string().required(),
                    message: Joi.string()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },
];