const nodemailer = require('nodemailer');
var authentication=require('../controllers/authentication');
var productService = require('../services/ProductService');  //RSG 02/13/2017 added a service
var checkoutService = require('../services/CheckoutService');
//var cartResource = require('../epbroker/CartResource');
var Joi= require('joi');
module.exports=[



    {
        method:'POST',
        path:'/vms/order/',
        config: {
            auth: false,
            handler: checkoutService.saveCreditScoreRangeType,
            description: 'Submit an order to VMS. Submit ',
            notes: 'A POST Request to store cart and customer ',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    //orderid: Joi.string().required(),
                    cart: Joi.object().required(),
                    customer: Joi.object().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    {
        method:'POST',
        path:'/checkout/billandship/',
        config:{
            auth:false,
            handler:checkoutService.saveBillAndShipInfo,
            description: 'Stores the customer shipping and billing information to the commercedb',
            notes: 'A POST Request Stores the customer shipping and billing information into the commercedb',
            tags: ['api'],
            plugins:{
                'hapi-swagger':{
                    responseMessages:[
                        {code:400,message:'Bad Request'},
                        {code:500,message:'Internal server error'}
                    ]
                }
            },
            validate:{
                payload:{

                    //orderid: Joi.string().required(),
                    //shiptype:Joi.string().required(),

                    lastname: Joi.string().required(),
                    firstname: Joi.string().required(),
                    address1: Joi.string().required(),
                    address2: Joi.string().required(),
                    city: Joi.string().required(),
                    state: Joi.string().required(),
                    phone: Joi.string().required(),
                    shipping_method: Joi.string().required(),
                    shipping_charge: Joi.string().required(),
                    fee: Joi.string().required(),

                    email: Joi.string().required(),

                    shipaddress1:Joi.string().required(),
                    shipaddress2:Joi.string().required(),
                    shipcity:Joi.string().required(),
                    shipstate:Joi.string().required(),
                    shipzip:Joi.string().required(),

                    billaddress1:Joi.string().required(),
                    billaddress2:Joi.string().required(),
                    billcity:Joi.string().required(),
                    billstate:Joi.string().required(),
                    billzip:Joi.string().required(),

                    cardno:Joi.string().required(),
                    expirydate:Joi.string().required(),
                    cvv: Joi.string().required(),
                    storeid: Joi.string().required(),       // system will add this
                    merchantid: Joi.string().required()     // system will add this

                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            },
        }
    },
    {
        method: 'POST',
        path: '/checkout/confirm/',
        config: {
            auth: false,
            handler: checkoutService.savePersonalInfo,
            description: 'Stores the customer confirmation with coupon information to the commercedb',
            notes: 'A POST Request Storing the customer coupon into the commercedb',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    coupon: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },

    /*
    Denominations by product id
Price,description,language
Fees by product is
Fee,description,exception if any


    {
        method:'GET',
        path:'/checkout/creditratingtypes/',
        config: {
            auth: false,
            handler: checkoutService.getCreditScoreRangeType,
            description: 'Get all possible credit score range type',
            notes: 'A GET Request to Get all possible credit score range type',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },
*/

];