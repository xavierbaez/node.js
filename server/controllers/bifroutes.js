const nodemailer = require('nodemailer');
var authentication=require('../controllers/authentication');
var bifService = require('../services/BifService');
var Joi= require('joi');
module.exports=[


    {
        method:'POST',
        path:'/services/V1/api/order/',
        config: {
            auth: false,
            handler: bifService.sendMagentoOrder_2_Bif,
            description: 'Submit a Magento order to BIF. Submit ',
            notes: 'A POST Request to submit Magento order and submit to BIF ',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    order: Joi.string().required()   // was object
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },


];