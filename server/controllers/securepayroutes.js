const nodemailer = require('nodemailer');
var authentication=require('../controllers/authentication');
var productService = require('../services/ProductService');  //RSG 02/13/2017 added a service
var checkoutService = require('../services/CheckoutService');
//var cartResource = require('../epbroker/CartResource');
var Joi= require('joi');
module.exports=[
    {
        method: 'POST',
        path: '/securepaybroker/fraudcheck/',
        config: {
            auth: false,
            handler: checkoutService.savePersonalInfo,
            description: 'Checks address and card details for a fraud',
            notes: 'A POST Request checking for fraud. They could be in the black list',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    lastname: Joi.string().required(),
                    firstname: Joi.string().required(),
                    address1: Joi.string().required(),
                    address2: Joi.string().required(),
                    city: Joi.string().required(),
                    state: Joi.string().required(),
                    phone: Joi.string().required(),
                    cardno:Joi.string().required(),
                    expirydate:Joi.string().required(),
                    cvv: Joi.string().required(),
                    zip: Joi.string().required
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },
    {
        method: 'POST',
        path: '/securepaybroker/pay/',
        config: {
            auth: false,
            handler: checkoutService.savePersonalInfo,
            description: 'Make a payment with order id from VMS/IDS',
            notes: 'A POST Request to submit a payment. This could be an IFRAME at the end',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            },
            validate: {
                payload: {
                    lastname: Joi.string().required(),
                    firstname: Joi.string().required(),
                    address1: Joi.string().required(),
                    address2: Joi.string().required(),
                    city: Joi.string().required(),
                    state: Joi.string().required(),
                    phone: Joi.string().required(),
                    cardno:Joi.string().required(),
                    expirydate:Joi.string().required(),
                    cvv: Joi.string().required(),
                    zip: Joi.string().required,
                    chargeamount: Joi.string().required()
                },
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown()
            }
        }
    },


];