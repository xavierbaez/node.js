const nodemailer = require('nodemailer');
var authentication=require('../controllers/authentication');
var productService = require('../services/ProductService');  //RSG 02/13/2017 added a service
var Joi= require('joi');
module.exports=[
    {
        method:'GET',
        path:'/magento/products',
        config: {
            auth: false,
            handler: productService.findAllMagentoProducts,
            description: 'Get all Magento products',
            notes: 'Returns all the products from our Magento',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: [
                        {code: 400, message: 'Bad Request'},
                        {code: 500, message: 'Internal server error'}
                    ]
                }
            }
        }
    },
];