var request = require('request');
var JWT = require('jsonwebtoken');
var querystring = require('querystring');

var decoded;
var base_uri = 'https://bifapi.test.com";';
var token = 'JX0rFY4K1q3QKmhtG/cX123123erHE=';
var test_uri = 'http://atlisewecor112v.unx.uqqs.net:27913/services/v1/api/order/createOrder';
/*
https://www.irs.gov/pub/irs-pdf/f1040.pdf
 */
module.exports = {



    /*
     *  Send Magento Order to Biff
     */

    sendMagentoOrder_2_Bif: function (auth_token, order, callback) {
        var message = 'In BIF sendMagentoOrder_2_BIF failure !!! ';
        var response = {'message': message, 'error': 500};

        console.log(' Will communicate to BIF system with end point=' + base_uri + '/services/order/api/' );
        console.log('Creating order for id=' + order);

        var bifOrder=this.createBIFOrder(order);

        var bif_uri = base_uri + '/services/order/api/';
        request({
            headers: {
                'Authorization': 'Bearer ' + auth_token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            uri: test_uri,
            method: 'POST',
            payload: bifOrder,
        }, function (err, res, body) {
            if (err) {
                callback(response);
                console.log(err);
            }
            else {
                console.log(body);
                console.log('POST to BIF send order successful..');
                callback(body);
            }
        });
    },



    /*
     *  Get brand new access token from BIF
     */
    getNewAccessToken: function (callback) {
        var message = 'BIF getNewAccessToken() failure!!!';
        var response = {'message': message, 'error': 500};

        console.log('In BifResource getNewAccessToken(): ');
        var postData = querystring.stringify({
            clientId:'ecomm',
            authorization:'84be1c365356e8ad04e8ae1bf9c3cd92fg49d0a7',   //'JX0rFY4K1q3QKmhtG/cXxynerHE=',
            createdTime:'2017-11-01 15:23:18.000090'
        });
        request({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': postData.length
            },
            uri: base_uri + '/auth/token',
            method: 'POST',
            body:postData

        }, function (err, res, body) {
            if (err) {
                callback('Failed getting the token from BIF!!!').code(500);
                console.log('Failed getting the token from BIF!!!');
                return;
            } else {
                console.log(body);
                var auth_res = JSON.parse(body);
                access_token = auth_res.access_token;
                token_type = auth_res.token_type;
                console.log('BIF getNewAccessToken() success..' + access_token);
                callback(access_token);
            }
        });
    },

    printMagentoOrder: function (order) {

        //order = JSON.parse(order);
        console.log('In printMagentoOrder');
        console.log(order.created_at);
        console.log(order.customer_email);
        console.log(order.quote_id); console.log(order.remote_ip); console.log(order.shipping_amount); console.log(order.shipping_description); console.log(order.state);
        console.log(order.status); console.log(order.store_currency_code); console.log(order.store_id);console.log(order.total_due); console.log(order.total_item_count);
        console.log(order.items);

        console.log('Printing order items');



        for (i in order.items) {

            console.log(order.items[i].created_at);
            console.log(order.items[i].item_id);
            console.log(order.items[i].name);
            console.log(order.items[i].order_id);
            console.log(order.items[i].price);
            console.log(order.items[i].product_id);
            console.log(order.items[i].product_type);
            console.log(order.items[i].qty_ordered);
            console.log(order.items[i].sku);
        };
        console.log('Printing address...');
        var shipping_assignments=order.extension_attributes.shipping_assignments[0];
        var address=shipping_assignments.shipping.address;
        console.log(address.city);
        console.log(address.company);
        console.log(address.email);
        console.log(address.firstname);
        console.log(address.lastname);
        console.log(address.postcode);
        console.log(address.region);
        console.log(address.region_code);
        console.log(address.street[0]);
        console.log(address.telephone);
        console.log('Out printMagentoOrder')

        return {status:1};

    },

    /*
     *  Create a BIF order from Magento Order
     */
    createBIFOrder: function (order) {


        var bifjson = {
                orders: []
        };
        var clientOrderNumberMagento= "MAG-000000005";      // RSG: Needs to be order_id from Magento

        var shipping_assignments=order.extension_attributes.shipping_assignments[0];
        var address=shipping_assignments.shipping.address;

        console.log('In createBIFOrder....');
        console.log('created_at=' + order.created_at);
        console.log('customer_email=' + order.customer_email);
        console.log('quote_id=' + order.quote_id);
        console.log('remote_ip=' + order.remote_ip);
        console.log('shipping_amount=' + order.shipping_amount);
        console.log('shipping_description=' + order.shipping_description);
        console.log('order_state=' + order.state);
        console.log('order_status='+ order.status);
        console.log('store_currency_code=' + order.store_currency_code);
        console.log('store_id=' + order.store_id);
        console.log('total_due=' + order.total_due);
        console.log('total_item_count=' + order.total_item_count);

        var biforder = {
                clientOrderNumber: clientOrderNumberMagento,
                programID: "Test_Peach",
                participantID: "127143",
                shipToFirstName: address.firstname,
                shipToMiddleInitial: "",
                shipToLastName: address.lastname,
                shipToAddressLine1:  address.street[0],
                shipToAddressLine2: "",
                shipToAddressLine3: "",
                shipToCity: address.city,
                shipToState: address.region_code,
                shipToPostalCode: address.postcode,
                shipToCountry: "USA",
                shipToPhone: address.telephone,
                shipToEmail: address.email,
                shipToCompanyName: "",
                orderLineItem : [],
        };


        for (i in order.items) {
                biforder.orderLineItem.push({
                    clientLineItemNumber: clientOrderNumberMagento + '_' + i,
                    offerCode: 'BIF-AERO',
                    quantity: order.items[i].qty_ordered,
                    shippingMethod: order.shipping_description,
                    packageID: order.items[i].sku,
                    productID: order.items[i].product_id,  // extra from our side
                    name: order.items[i].name,             // extra from our side
                    activationCode: '',
                    denomination: order.price,
                    logoID: '',
                    companyName: '',
                    imageID: '',
                    embossedLine: '',
                    carrierMessage: '',
                    carrierMessageClosing: '',
                    carrierMessageSignature: ''});
                i++;
        }
        bifjson.orders.push(biforder);
        console.log(bifjson);

        return bifjson;

    },

}













