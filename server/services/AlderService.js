var productResource = require('../magentobroker/ProductResource');
var vmsResource = require('../vms/VmsResource');
var JWT   = require('jsonwebtoken');
var decoded;
module.exports={


    /*
      List all the products in Magento
     */

    findAllMagentoProducts: function(request,response) {
           var data='test';
           productResource.findAllCatalogItems(data, function (cdata) {
               response(cdata).code(200);
           });
    },

    findAllProducts: function(request,response)  {

       vmsResource.getNewAccessToken(function(data){
             vmsResource.getCatalogs(data,'3425',function (cdata) {
                response(cdata).code(200);
            });

        });

    },

    postStatus: function (request,response) {
        productResource.updateStatus('000000005', function (data) {
            response('{ok}').code(200);
        });
    },

    findAllAssets: function(request,response)  {

        //vmsResource.getNewAccessToken(function(data){
        var data = 'P6RNR9w-dScZa_8Dpbx102M7wvqJvnttN5CHulblOkIJHopO4FYQsPiGpWfqSd_g5_yA5ameuF5tyVBDZeuIpmpU90I5T8r_LVKjO7exh06_tfYkxc49WaNPqFV6icm-GnfqEplfwZ_HgiB_VA5sLBKDCipIHjkgldjgCMNRHcjmxmSLIOgUySBeiHst_xeRPZxpJc7Xw1NbwWX-Fr44oIDL3iPH69yYbrs3eX2_sOq-vF6AK10e9Lo9qMikRCXkwP-Dr0dvbkYASZyB6q1nDhlIwi00ZazRUt-jBOpVTzJgYMC8EdfOzwXvREnhUQMYkOJ8C_ZOBfP9y7Lj7I-MXMg6uoS8FbTNV6pOMSFCrrWsJa__01s7mmn55ePmbXXzt037z_PopuDI6izOMTL-g-quQ6t6VTk0Th-xF5FnfdE6UZHY94pTFxWHC1LdrztzEcDJ3mBdmyHVEgwSpimo0w';
        vmsResource.getAssets(data,'3425',function (cdata) {
            response(cdata).code(200);
        });

        //});

    },

    findAllProductsByStoreAndMerchant: function(request,response) {
        var storeId = request.params.storeid;
        var merchantId = request.params.merchantId;
        productResource.findAllProductsByStoreAndMerchant(storeId,merchantId,response);
    },

    findDenominationsByProductId: function (request, response) {
        var productId = request.params.productid;
        productResource.findDenominationsByProductId(productId,response);
    },

    findFeesByProductId: function (request, response) {
        var productId = request.params.productid;
        productResource.findFeesByProductId(productId,response);
    },
    /* Security InfoSec */
/*
<?php>
    class Test_SendOrder_Model_Observer() {
       public updateOrder ($observer) {
            $order = $observer->getEvent()->getOrder();
            //add code to convert to json
            $order_json = json_encode($order);
            //API Url
            $url = 'http://localhost:3000/services/v1/api/tobiff';
            //Initiate cURL.
            $ch = curl_init($url);
            //The JSON data.
            $jsonData = array('token' => 'Bearer 12fdhg');
            //Encode the array into JSON.
            $jsonDataEncoded = json_encode($order);

            //Tell cURL that we want to send a POST request.
            curl_setopt($ch, CURLOPT_POST, 1);

            //Attach our encoded JSON string to the POST fields.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

            //Set the content type to application/json
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

            //Execute the request
            $result = curl_exec($ch);
       }
 }
?>

*/
};
