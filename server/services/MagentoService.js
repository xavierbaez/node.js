var orderResource = require('../magentobroker/OrderResource');
var productResource = require('../magentobroker/ProductResource');
var JWT   = require('jsonwebtoken');
var decoded;
module.exports={

        /*
        * get Magento order using REST API
        */
        getMagentoOrderById: function(request,response) {
               var orderId='00000005';
               orderResource.getMagentoOrderById(orderId, function (data) {
                   response(data).code(200);
               });
        },

        /*
        * find list of magento products using REST API
        */
        findMagentoProductsByCategory: function(request,response) {
                var category='Gift Card';
                productResource.findAllProducts(category, function (data) {
                    response(data).code(200);
                });
        },

        /*
        * Update order based on BIF notification
        */
        updateMagentoOrder: function (request,response) {
                orderResource.updateOrderStatus('000000005', function (data) {
                    response('{ok}').code(200);
                });
        },

};
