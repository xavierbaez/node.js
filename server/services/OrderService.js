var orderResource = require('../magentobroker/OrderResource');
var JWT   = require('jsonwebtoken');
var decoded;
module.exports={

        /*
        * Get a Order using Magento REST API
        */
        getMagentoOrderById: function(request,response) {
               var orderId=request.params.orderid;
               orderResource.getMagentoOrderById(orderId, function (data) {
                   console.log(data);
                   response(data).code(200);
               });
        },



        /*
        * Update Order Status and Comment in Magento from  BIF  incoming status notification
        */
        updateMagentoOrder: function (request,response) {

            var access_token = '';
            var headers = request.headers;
            console.log('Authorization Token : ' + headers.authorization);
            var payload = request.payload;
            console.log('Incoming payload=' + payload);
            var jsonOrder = payload;      // Looking for a full notification object for every sub order
                                          // The message attribute could be anything from them

            //var jsonOrder = JSON.parse(require('fs').readFileSync('server/testfiles/biffsamplenotification.json', 'utf8'));

            orderResource.updateOrderStatus(jsonOrder, function (data) {
                    console.log('Data in service=' + data);
                    response("{}");
                });
        },

};
